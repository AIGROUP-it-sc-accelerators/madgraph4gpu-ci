#!/bin/bash

# Checks if variables have been set, if not assign default
  REPO_URL=${REPO_URL:-'https://github.com/madgraph5/madgraph4gpu'}
  RUNNER_NAME=${RUNNER_NAME:-'github_runner'}
  GITHUB_RUNNER_TAGS=${GITHUB_RUNNER_TAGS:-'Linux,x64'}
  RUNNER_URL=${RUNNER_URL:-'https://github.com/actions/runner/releases/download/v2.303.0/actions-runner-linux-x64-2.303.0.tar.gz'}

  RUNNER=$(pwd)/actions-runner/run.sh

  # Checks if GITHUB_TOKEN has not been set, exists if not
  if [ -z "${GITHUB_TOKEN}" ]; then echo "Need GITHUB_TOKEN for GitHub Runner setup!" && exit 1; fi

  # To get all profile.d scripts as an interactive session
  for script in /etc/profile.d/*.sh; do
      if [ -r $script ]; then
          . $script
      fi
  done

  # Configure GitHub Runner
  if [ ! -f "$(pwd)/actions-runner/.runner" ]; then
  mkdir $(pwd)/actions-runner && cd $(pwd)/actions-runner
  curl -o ./actions-runner.tar.gz -L ${RUNNER_URL}
  tar -xzf ./actions-runner.tar.gz
  ./config.sh --unattended --url ${REPO_URL} --token ${GITHUB_TOKEN} --replace --name ${RUNNER_NAME} --labels ${GITHUB_RUNNER_TAGS}
  fi

  while true; do
  if ! pgrep -f ${RUNNER} > /dev/null 2>&1; then
      # Runner hasn't been started yet or exited because of failure / update
      ${RUNNER}
  else
      # Runner was restarted, and is running in background. Let's wait for it.
      PID=$(pgrep -f ${RUNNER}) && tail --pid=$PID -f /dev/null
  fi
  sleep 10
  done