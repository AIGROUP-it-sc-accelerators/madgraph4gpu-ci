#!/bin/bash

# TODO
# Script in /usr/bin find a place to keep it
# Secrets in a file that only root can read and get it from there instead of writing it in cleartext in the script

# Variables
CONTAINER_NAME="github_runner"
IMAGE_URL="ghcr.io/jooorgen/github_runner:latest"
REPO="Jooorgen/madgraph4gpu"
REPO_URL="https://github.com/Jooorgen/madgraph4gpu"
GITHUB_RUNNER_TAGS="Linux,x64,a100"

# Gets pat from /etc/github_pat
PAT=`cat /etc/github_pat`

# Function to start the Podman container
start_container() {
    # Check if the container exists with the given name
    if podman ps -a --format '{{.Names}}' | grep -q "^$CONTAINER_NAME$"; then
        # Attempt to start the existing container
        podman start $CONTAINER_NAME

        # Check the return code of the podman start command
        if [ $? -ne 0 ]; then
            echo "Error: Failed to restart the container '$CONTAINER_NAME'."
            exit 1
        else
            echo "Container '$CONTAINER_NAME' started successfully."
            exit 0
        fi
    else
        # Pull the latest Podman image
        podman login ghcr.io -p $PAT
        podman pull $IMAGE_URL

        TOKEN=$(curl -XPOST -fsSL \
                        -H "Authorization: token $PAT " \
                        -H "Accept: application/vnd.github.v3+json" \
                        "https://api.github.com/repos/$REPO/actions/runners/registration-token" \
                | grep -o '"token": *"[^"]*"' | cut -d '"' -f 4)

        # Start a new container using the latest image and specific environment variables
        podman run -d \
            --name $CONTAINER_NAME \
            --security-opt=label=disable \
            -e GITHUB_TOKEN=$TOKEN \
            -e REPO_URL=$REPO_URL \
            -e GITHUB_RUNNER_TAGS=$GITHUB_RUNNER_TAGS \
            -e RUNNER_NAME=$CONTAINER_NAME \
            $CONTAINER_NAME

        if [ $? -ne 0 ]; then
            echo "Error: Failed to create and start the container '$CONTAINER_NAME'."
            exit 1
        else
            echo "Container '$CONTAINER_NAME' created and started successfully."
            exit 0
        fi
    fi
}


# Function to stop the podman container
stop_container() {
    if podman ps -a --format '{{.Names}}' | grep -q "^$CONTAINER_NAME$"; then
        podman stop $CONTAINER_NAME
    else
        echo "Container $CONTAINER_NAME was stopped."
    fi
}

# Check for arguments to decide whether to start or stop the container
case "$1" in
    start)
        start_container
        ;;
    stop)
        stop_container
        ;;
    *)
        echo "Usage: $0 {start|stop}"
        exit 1
esac