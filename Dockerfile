FROM almalinux:8
LABEL maintaner="Stephan/Jorgen"

# Docker arguments
ARG CUDA_VERSION=12-0
ARG CUDA_DIR_NAME=cuda-12.0
ARG GCC_VERSION=11.2.1
ARG GPU=a100

# Perform installation as root
USER root
WORKDIR /root

# Install basic development enviroment
RUN yum install -y gcc which git libicu lttng-ust vim nano wget unzip gcc-toolset-11 glibc glibc-devel \
                   clang python39 ncurses ncurses-devel dnf-plugins-core tar gcc-gfortran epel-release
ARG GCC_ROOT=/opt/rh/gcc-toolset-11/root/usr
COPY docker/gcc-toolset-11.sh /etc/profile.d/

# CUDA 12.0 Setup
RUN dnf config-manager --add-repo https://developer.download.nvidia.com/compute/cuda/repos/rhel8/x86_64/cuda-rhel8.repo && \
    dnf module enable -y nvidia-driver:530-dkms && \
    dnf module install -y nvidia-driver && \
    yum install -y cuda-${CUDA_VERSION}
ENV CUDA_PATH=/usr/local/${CUDA_DIR_NAME}
ENV NVIDIA_VISIBLE_DEVICES=all
COPY docker/cuda-12.0.sh /etc/profile.d/

# ROCm 5.2.5 Setup
COPY docker/rocm-5.2.5.repo /etc/yum.repos.d/
COPY docker/amdgpu.repo /etc/yum.repos.d/
RUN dnf config-manager --set-enabled powertools &&                        \
    dnf config-manager --enable epel &&                                   \
    dnf install -y rocm-hip-libraries amdgpu-dkms &&                      \
    ln -s rocm-5.2.5 /opt/rocm &&                                         \
    dnf clean all
COPY docker/rocm-5.2.5.sh /etc/profile.d/

# CMake 3.21.3 Setup
RUN wget https://cmake.org/files/v3.21/cmake-3.21.3-linux-x86_64.tar.gz &&     \
    mkdir -p /opt/cmake/3.21.3/Linux-x86_64 &&                                 \
    tar -C /opt/cmake/3.21.3/Linux-x86_64 --strip-components=1                 \
        --no-same-owner -xvf cmake-*-linux-x86_64.tar.gz &&                    \
    rm cmake-*-linux-x86_64.tar.gz
COPY docker/cmake-3.21.3.sh /etc/profile.d/

# Ninja 1.11.1 Setup
RUN wget https://github.com/ninja-build/ninja/releases/download/v1.11.1/ninja-linux.zip && \
    mkdir -p /opt/ninja/1.11.1/Linux-x86_64 &&                                             \
    unzip ninja-linux.zip -d /opt/ninja/1.11.1/Linux-x86_64 &&                             \
    chmod 755 /opt/ninja/*/Linux-x86_64/ninja &&                                           \
    rm ninja-linux.zip
COPY docker/ninja-1.11.1.sh /etc/profile.d/

# LLVM DPCPP (Commit fea99cc9ad67) Setup
# ENV DPCPP_HOME=/opt/sycl_workspace
# ARG COMMIT_SHORT=fea99cc9ad67
# ARG DATE_FORMAT=20230412
# ARG LLVM_INSTALL_NAME=llvm-${DATE_FORMAT}-${COMMIT_SHORT}-gcc-${GCC_VERSION}-cuda-${CUDA_VERSION}-gpu_${GPU}
# ENV LLVM_INSTALL_PATH=/opt/llvm/${LLVM_INSTALL_NAME}

# RUN mkdir ${DPCPP_HOME}
# WORKDIR ${DPCPP_HOME}
# RUN git clone https://github.com/intel/llvm -b sycl
# WORKDIR ${DPCPP_HOME}/llvm
# RUN git checkout fea99cc9ad67

# RUN source /opt/rh/gcc-toolset-11/enable &&                                         \
#     PATH=/opt/cmake/3.21.3/Linux-x86_64/bin${PATH:+:${PATH}} &&                     \
#     PATH=/opt/ninja/1.11.1/Linux-x86_64${PATH:+:${PATH}} &&                         \
#     python3 ${DPCPP_HOME}/llvm/buildbot/configure.py --cuda -o ${LLVM_INSTALL_PATH} \
#         --cmake-opt="-DCUDA_TOOLKIT_ROOT_DIR=${CUDA_PATH}"                          \
#         --cmake-opt="-DSYCL_LIBDEVICE_GCC_TOOLCHAIN=${GCC_ROOT}"                    \
#         --cmake-opt="-DGCC_INSTALL_PREFIX=${GCC_ROOT}" &&                           \
#     python3 ${DPCPP_HOME}/llvm/buildbot/compile.py -o ${LLVM_INSTALL_PATH} -j 64 && \
#     echo "${LLVM_INSTALL_PATH}/lib" > /etc/ld.so.conf.d/intel-clang.conf
# COPY docker/intel-dpcpp.sh /etc/profile.d/

# Entrypoint
RUN useradd CI
WORKDIR /home/CI/
COPY ./entrypoint.sh .

RUN chown CI ./entrypoint.sh && ls -l && chmod u+x ./entrypoint.sh

USER CI
CMD [ "./entrypoint.sh" ]